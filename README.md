ELTE IK Szakdolgozat

Cím: Többszereplős Snake játék
Témavezető: Dr. Törley Gábor
Készítette: Keller Kitti Bernadett

A játék a jól ismert Snake (kígyó) játékon alapul annyi különbséggel, hogy itt már nem csak egyedül, hanem egyszerre többen is részt vehetnek
a játékban és versenyezhetnek egymás ellen.

A játékba való belépés előtt a játékosnak lehetősége van a regisztrálásra,így a rendszer megjelenítheti a nevét, de név megadása 
nélkül is játszhatunk ekkor legenerálodig egy 6karakter hosszú betűből és számból álló név.A játék során a játokosok felkerülnek a rangsor 
táblára.A játékban még lehetőségünk van a kígyó bőrszínének a kiválasztására is

A kígyó irányítása a billentyűzett nyilaival, illetve az a w s d billentyűk lenyomásával lehetséges.

A - bal; W-fel; S-le; D-jobb

A pályán különféle gyümölcsök helyezkednek el, illetve mozgó kisebb állatok, melyeket ha megeszi a kígyónk nagyobbra hízik illetve 1 
vagy 2 pontot szerzünk. Mindenki először 5 egység hosszú kígyóval kezd, és nagyságra nincs felső korlát.

Emellett bombák is találhatóak a pályán melyek elfogyasztása zsugorodással jár. Ha 2 egység alá zsugorodunk, a kígyónk éhen hal 
veszítünk. Emellett ha a kígyónkkal magunkat harapjuk meg az is pont vesztéssel, illetve méret csökkenéssel jár

A játék akkor ér véget ha -3 pont alá menne a pontunk, vagy ha falnak ütközünk.

A játék lényege, hogy a kígyónkkal minnél tovább éljünk túl és minnél nagyobbra nöjjünk.

Jó szórakozást kívánok!

![alt text](img/drotvazterv.png "Főoldal")
![alt text](img/jatek.png "Játék oldal")
![alt text](img/aJatekrol.PNG "Leírás oldal")
![alt text](img/kapcsolat.png "Kapcsolat oldal")
![alt text](img/offlineTerv.png "Offline oldal")
