import unittest
from PythonWeb import app

class Test_test_1(unittest.TestCase):
    def test_A(self):
        tester = app.test_client(self)
        response = tester.get('/',content_type='html/text')
        self.assertEqual(response.status_code,200)

    def test_home(self):
        tester = app.test_client(self)
        response = tester.get('/',content_type='html/text')
        self.assertTrue(b'Az eredeti' in response.data)

    def test_login(self):
        tester = app.test_client(self)
        response = tester.post('/home',
                               data=dict(username="admin", email="admin@gmail.com"),
                               follow_redirects = True)
        self.assertIn(b'Ad meg a neved', response.data)

    def test_play(self):
        tester = app.test_client(self)
        response = tester.get('/play',content_type='html/text')
        self.assertTrue(b'rangsor:' in response.data)

    def test_offline(self):
        tester = app.test_client(self)
        response = tester.get('/offline',content_type='html/text')
        self.assertTrue(b'Pontok:' in response.data)

    def test_contact(self):
        tester = app.test_client(self)
        response = tester.get('/contact',content_type='html/text')
        self.assertTrue(b'Keller Kitti Bernadett' in response.data)
    
    def test_about(self):
        tester = app.test_client(self)
        response = tester.get('/about',content_type='html/text')
        self.assertTrue(b'A - bal; W-fel; S-le; D-jobb' in response.data)

if __name__ == '__main__':
    unittest.main()
