"""
Routes and views for the flask application.
"""

from datetime import datetime, timedelta
from flask import render_template, request, redirect, session, url_for, jsonify, make_response, json
from PythonWeb import app
import random
import threading
import time
import random

class Bug(threading.Thread):
    def __init__(self):
        super().__init__()
        self.daemon = True
        self.X=random.randint(0,64)
        self.Y=random.randint(0,19)

    def run(self):
        while True:
            whichDir = random.randint(0,1)
            if self.X>=65:
                rand=random.randint(0,30)
                self.X=rand
            elif self.Y>=20:
                rand=random.randint(0,10)
                self.Y=rand
            if whichDir==0:
                self.X = self.X+1 
            else:
                self.Y = self.Y+1
            time.sleep(5)
            


@app.route('/')
@app.route('/home', methods=['POST'])
def home():
    """Renders the home page."""
    return render_template(
        'index.html',
        title='Home Page',
        year=datetime.now().year,
        title1='Többszereplős kígyó játék', #TODO vagy magyar vagy angol legyen -> inkabb angol
        header='Az eredeti kígyójáték többszereplős változata.',
        guide='Ad meg a neved vagy csak kattints a Csatlakozás gombra.',
        Username='Felhasználó neved',
        Connect='Csatlakozás',
        OnePlayer='Egyjátékos mód'
    )


@app.route('/contact')
def contact():
    """Renders the contact page."""
    return render_template(
        'contact.html',
        title='Kapcsolat',
        year=datetime.now().year,
        message='Készítette:'
    )

@app.route('/about')
def about():
    """Renders the about page."""
    return render_template(
        'about.html',
        title='Többszereplős kígyó Játék leírása',
        year=datetime.now().year,
        message='Használat:',
        lines=['A játék a jól ismert Snake (kígyó) játékon alapul annyi különbséggel, hogy itt már nem csak egyedül, hanem egyszerre többen is részt vehetnek a játékban és versenyezhetnek egymás ellen.',
               'A játékba való belépés előtt a játékosnak lehetősége van a regisztrálásra,így a rendszer megjelenítheti a nevét, de név megadása nélkül is játszhatunk ekkor legenerálodig egy 6karakter hosszú betűből és számból álló név.A játék során a játokosok felkerülnek a rangsor táblára.A játékban még lehetőségünk van a kígyó bőrszínének a kiválasztására is',
               'A kígyó irányítása a billentyűzett nyilaival, illetve az a w s d billentyűk lenyomásával lehetséges.',
               'A - bal; W-fel; S-le; D-jobb',
               'A pályán különféle gyümölcsök helyezkednek el, illetve mozgó kisebb állatok, melyeket ha megeszi a kígyónk nagyobbra hízik illetve 1 vagy 2 pontot szerzünk. Mindenki először 5 egység hosszú kígyóval kezd, és nagyságra nincs felső korlát.',
               'Emellett bombák is találhatóak a pályán melyek elfogyasztása zsugorodással jár. Ha 2 egység alá zsugorodunk, a kígyónk éhen hal veszítünk. Emellett ha a kígyónkkal magunkat harapjuk meg az is pont vesztéssel, illetve méret csökkenéssel jár',
               'A játék akkor ér véget ha -3 pont alá menne a pontunk, vagy ha falnak ütközünk.',
               'A játék lényege, hogy a kígyónkkal minnél tovább éljünk túl és minnél nagyobbra nöjjünk.',
               'Jó szórakozást kívánok!'],
    )

@app.route('/offline')
def offline():
    """Renders the about page."""
    return render_template(
        'offline.html',
        title='Offline egyéni játék.',
        year=datetime.now().year,
        message='Használat:'
    )

@app.route('/play')
def play():
  global name
  name = request.args.get('username')
  return render_template('play.html')

def changeMap(objectName,objectBool,gameObjects,values,user,glob):
    objName = request.form[objectName]
    Eaten = request.form[objectBool]
    global FruitsCoords, BombsCoords

    if objName:
      objName = [int(s) for s in objName.split(',')]
      objName = nest_list(objName,int(len(objName)/2),2)
    gameObjects[user] = [objName,Eaten]
    values = gameObjects.items()
    values = list(values)
    if len(values)==1:
        if glob:
           if objName and objName!=FruitsCoords:
              FruitsCoords = objName
        else:
            if objName and objName!=BombsCoords:
              BombsCoords = objName
    
    if objName and len(values)>=2:
        for i in range(len(values)):
          for j in range(i + 1, len(values)):
              if values[i][1][0] and values[j][1][0]:
                  if values[i][1][0]!=values[j][1][0]:
                      if values[i][1][1]=='true':
                          if glob:
                            FruitsCoords = values[i][1][0]
                          else:
                              BombsCoords = values[i][1][0]
                      if values[j][1][1]=='true':
                          if glob:
                            FruitsCoords = values[j][1][0]
                          else:
                              BombsCoords = values[j][1][0]

gameObjects = {}
values = [[]]
@app.route("/updateFruits/<user>", methods=['POST'])
def UpdateFruits(user):
    changeMap('Fruits','fruitEaten',gameObjects,values,user,True)
    global FruitsCoords

    data={ 'fruits':FruitsCoords, 'fruitEaten':False} 
    return jsonify(data)

gameBombs = {}
valuesBomb = [[]]
@app.route("/updateBombs/<user>", methods=['POST'])
def UpdateBombs(user):

    changeMap('bombak','bombEaten',gameBombs,valuesBomb,user,False)
    global BombsCoords

    data={ 'bombs': BombsCoords, 'bombEaten':False} 
    return jsonify(data)

gameBugs = {}
valuesBug = [[]]

bug1 = Bug()
bug2 = Bug()
bug3 = Bug()

@app.route("/updateBugs/<user>", methods=['POST'])
def UpdateBugs(user):

  bu = request.form['bugs']
  bugEaten = request.form['bugEaten']
  index = request.form['index']

  BugsCoords = [[bug1.X,bug1.Y],[bug2.X,bug2.Y],[bug3.X,bug3.Y]]

  if bu:
      bu = [int(s) for s in bu.split(',')]
      bu = nest_list(bu,int(len(bu)/2),2)
  gameBugs[user] = [bu,bugEaten]
  valuesBug = gameBugs.items()
  valuesBug = list(valuesBug)
  if bu and len(valuesBug)==1:
      if bugEaten=="true":
          if index :
            index=int(index)
          if index==0:
              bug1.X=bu[0][0]
              bug1.Y=bu[0][1]
          elif index==1:
              bug2.X=bu[1][0]
              bug2.Y=bu[1][1]
          else:
              bug3.X=bu[2][0]
              bug3.Y=bu[2][1]
          BugsCoords = bu
          index=[]

  if bu and len(valuesBug)>=2:
      for i in range(len(valuesBug)):
        for j in range(i + 1, len(valuesBug)):
            if valuesBug[i][1][0] and valuesBug[j][1][0]:
                if valuesBug[i][1][0]!=valuesBug[j][1][0]:
                    if valuesBug[i][1][1]=='true':
                        if index :
                            index=int(index)
                        if index==0:
                            bug1.X=valuesBug[i][1][0][0][0]
                            bug1.Y=valuesBug[i][1][0][0][1]
                        elif index==1:
                            bug2.X=valuesBug[i][1][0][1][0]
                            bug2.Y=valuesBug[i][1][0][1][1]
                        else:
                            bug3.X=valuesBug[i][1][0][2][0]
                            bug3.Y=valuesBug[i][1][0][2][1]
                        BugsCoords = valuesBug[i][1][0]
                        index=[]

                    if valuesBug[j][1][1]=='true':
                        if index :
                            index=int(index)
                        if index==0:
                            bug1.X=valuesBug[i][1][0][0][0]
                            bug1.Y=valuesBug[i][1][0][0][1]
                        elif index==1:
                            bug2.X=valuesBug[i][1][0][1][0]
                            bug2.Y=valuesBug[i][1][0][1][1]
                        else:
                            bug3.X=valuesBug[i][1][0][2][0]
                            bug3.Y=valuesBug[i][1][0][2][1]
                        BugsCoords = valuesBug[j][1][0]
                        index=[]


  data={ 'bogarXY': BugsCoords, 'bugEaten':False, 'eatenBugIndex': index} 
  return jsonify(data)

gameDict = {}
snakes={}

@app.route("/players/<user>", methods=['POST'])
def updatePlayers(user):
    global snakes, gamePoints
    trail = request.form['trail']
    if trail:
      trailArray = [int(s) for s in trail.split(',')]
      trailArray = nest_list(trailArray,int(len(trailArray)/2),2)
    else:
      trailArray = []
    gameDict[user] = trailArray

    date = datetime.now()
    snakes[user] = date

    for key in snakes:
        if snakes[key]:
            if snakes[key] + timedelta(0,5) < date:
                snakes[key]=''
                gameDict.pop(key)
                gamePoints.pop(key)

    # Only return the other user, not ourself.
    gameDictReturn = gameDict.copy()
    gameDictReturn.pop(user)

    data={'trails': gameDictReturn}
    return jsonify(data)  

gamePoints = {}

@app.route("/scores/<user>", methods=['POST'])
def updateScore(user):
    score = request.form['myPoint']
    gamePoints[user] = int(score)

    sorted_points = {}
    for i in sorted(gamePoints, key=gamePoints.get):
        sorted_points[i] = gamePoints[i]

    dictlist = []
    for key, value in sorted_points.items():
        dictlist.append([key,value])
    dictlist.reverse()

    data={'Points' : dictlist}
    return jsonify(data)

Walls = [[1,10],[1,9],[1,8],[60,11],[61,11],[61,12],[20,5],[21,5],[22,5],[23,5],[23,6],[40,10],[41,9],[42,9],[34,15],[35,15],[36,15],[37,15]]

@app.route("/walls/<user>", methods=['POST'] )
def passWalls(user):
    global Walls
    data = {'walls' : Walls}
    return jsonify(data)

def nest_list(list1,rows, columns):    
    result=[]               
    start = 0
    end = columns
    for i in range(rows): 
        result.append(list1[start:end])
        start +=columns
        end += columns
    return result

def randFixFruits():
    Fruits = [[0,0],[0,0],[0,0],[0,0],[0,0]]

    fru = random.sample(range(1, 65), 5)
    for i in range(0,5):
        Fruits[i][0] = fru[i] 

    fru = random.sample(range(1, 20), 5)
    for i in range(0,5):
        Fruits[i][1] = fru[i] 
    
    for i in range(0,len(Walls)):
        if Walls[i] in Fruits:
            randFixFruits()
                    
    return Fruits

FruitsCoords = randFixFruits()

def randFixBomb():
    Bombs = [[0,0],[0,0],[0,0],[0,0],[0,0]]
    b = random.sample(range(1, 65), 5)
    for i in range(0,5):
        Bombs[i][0] = b[i] 

    b = random.sample(range(1, 20), 5)
    for i in range(0,5):
        Bombs[i][1] = b[i] 

    for i in range(0,len(Walls)):
        if Walls[i] in Bombs:
             randFixBomb()
    return Bombs

BombsCoords = randFixBomb()


