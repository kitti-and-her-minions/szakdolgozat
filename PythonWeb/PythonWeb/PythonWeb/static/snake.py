from browser import document, html, window, ajax
from javascript import Math

import json

import random

class Game:
    def __init__(self):
        self.gamePoints = {}
        self.gameStatus = {}
        self.gameStatus['fruits'] = []
        self.gameStatus['bombs'] = []
        self.gameStatus['bugs'] = []
        self.gameStatus['bugEaten'] = False
        self.gameStatus['eatenBugIndex'] =[]
        self.gameStatus['fruitEaten'] = False
        self.gameStatus['bombEaten'] = False

        self.user=site.get_me()
        self.id = random.randint(0,100)

        self.gameStatus['walls'] = []

    def handleScores(self):
        def on_complete(req):
            if req.status == 200 or req.status == 0:
                
                self.gamePoints['points'] = json.loads(req.text)['Points']
            
        ajax.post('/scores/'+str(self.user)+str(self.id), data={'myPoint': gameSnake.score }, oncomplete=on_complete)

    def BugUpdate(self):
        def on_complete(req):
           if req.status == 200 or req.status == 0:                         
               self.gameStatus['bugs'] = json.loads(req.text)['bogarXY']

               self.gameStatus['bugEaten'] = json.loads(req.text)['bugEaten']

               self.gameStatus['eatenBugIndex'] = json.loads(req.text)['eatenBugIndex']

        ajax.post('/updateBugs/'+str(self.user)+str(self.id), data={'bugs':self.gameStatus['bugs'], 'bugEaten':self.gameStatus['bugEaten'], 'index':self.gameStatus['eatenBugIndex'] }, oncomplete=on_complete)  

    def FruitUpdate(self):
        def on_complete(req):
           if req.status == 200 or req.status == 0:           
               self.gameStatus['fruits'] = json.loads(req.text)['fruits']

               self.gameStatus['fruitEaten'] = json.loads(req.text)['fruitEaten']

        ajax.post('/updateFruits/'+str(self.user)+str(self.id), data={'Fruits':self.gameStatus['fruits'], 'fruitEaten':self.gameStatus['fruitEaten'] }, oncomplete=on_complete)  

    def BombUpdate(self):
        def on_complete(req):
           if req.status == 200 or req.status == 0:           
               self.gameStatus['bombs'] = json.loads(req.text)['bombs']

               self.gameStatus['bombEaten'] = json.loads(req.text)['bombEaten']

        ajax.post('/updateBombs/'+str(self.user)+str(self.id), data={'bombak':self.gameStatus['bombs'], 'bombEaten':self.gameStatus['bombEaten'] }, oncomplete=on_complete)  


    def otherPlayers(self):
        def on_complete(req):
            if req.status == 200 or req.status == 0:
                self.gameStatus['trails'] = json.loads(req.text)['trails']

        ajax.post('/players/'+str(self.user)+str(self.id), data={'trail': gameSnake.trail }, oncomplete=on_complete)  

    def getWalls(self):
        def on_complete(req):
            if req.status == 200 or req.status == 0:
                self.gameStatus['walls'] = json.loads(req.text)['walls']
        
        ajax.post('/walls/'+str(self.user)+str(self.id), data={}, oncomplete=on_complete) 

class Snake:
    def __init__(self):
        self.score = 0
        self.high_score = 0
        self.SnakeX = self.SnakeY = 10 
        self.BoardY =  20 
        self.BoardX = 65
        self.xv = +1
        self.yv = 0
        self.trail = []
        self.tail = 5
        
        self.mySnakeColor = "lime"

    ############### Draw function ####################
    def draw(self,array,color):
        ctx.fillStyle = color
        for i in range(0,len(array)):
            ctx.fillRect(array[i][0]*self.BoardY, array[i][1]*self.BoardY, self.BoardY-2, self.BoardY-2)

    ############## Draw other snakes #########################

    def drawOtherSnakes(self):
        if 'trails' in game.gameStatus:
            for trail in game.gameStatus['trails'].keys():
                self.draw(game.gameStatus['trails'][trail],"yellow")      
    
    def changeObjPos(self,array,eaten,point,other):
        for i in range(0,len(array)):
            if array[i][0] == self.SnakeX and array[i][1] == self.SnakeY:
                game.gameStatus[eaten] = True
                self.tail += point
                ctx.fillStyle = "black"
                ctx.fillRect(array[i][0]*self.BoardY,array[i][1]*self.BoardY, self.BoardY-2, self.BoardY-2)
                array[i][0] = Math.floor(Math.random()*self.BoardX)
                array[i][1] = Math.floor(Math.random()*self.BoardY)
                while array[i] in game.gameStatus['walls'] or array[i] in game.gameStatus[other]:
                     array[i][0] = Math.floor(Math.random()*self.BoardX)
                     array[i][1] = Math.floor(Math.random()*self.BoardY)
                self.score += point

    def game(self):

        self.SnakeX += self.xv
        self.SnakeY += self.yv
        if self.SnakeX < 0:
            self.SnakeX = self.BoardX-1
        if self.SnakeX > self.BoardX-1:
            self.SnakeX = 0
        if self.SnakeY < 0:
            self.SnakeY = self.BoardY-1
        if self.SnakeY > self.BoardY-1:
            self.SnakeY = 0
    
        ctx.fillStyle = "black"
        ctx.fillRect(0, 0, canvas.width, canvas.height)

        ctx.fillStyle = self.mySnakeColor
        for i in range(0,len(self.trail)):
            ctx.fillRect(self.trail[i][0]*self.BoardY, self.trail[i][1]*self.BoardY, self.BoardY-2, self.BoardY-2)

        self.trail.insert(0, [self.SnakeX, self.SnakeY])
        while len(self.trail) > self.tail:
            self.trail.pop()
    
        self.changeObjPos(game.gameStatus['fruits'],'fruitEaten',1,'bombs')
        self.draw(game.gameStatus['fruits'],"pink")
        self.changeObjPos(game.gameStatus['bombs'],'bombEaten',1,'fruits')
        self.draw(game.gameStatus['bombs'],"gray")
    
        for i in range(0,len(game.gameStatus['bugs'])):
            if game.gameStatus['bugs'][i][0] == self.SnakeX and game.gameStatus['bugs'][i][1] == self.SnakeY:
                game.gameStatus['bugEaten'] = True
                game.gameStatus['eatenBugIndex'] = i
                self.score += 2
                self.tail += 1
                game.gameStatus['bugs'][i][0] = Math.floor(Math.random()*self.BoardX)
                game.gameStatus['bugs'][i][1] = Math.floor(Math.random()*self.BoardY)
        self.draw(game.gameStatus['bugs'],"red")

        self.draw(game.gameStatus['walls'],"brown")

        site.update_score(self.score)

        for i in range(0,len(game.gameStatus['walls'])):
            if game.gameStatus['walls'][i][0] == self.SnakeX and game.gameStatus['walls'][i][1] == self.SnakeY:
                site.GameOver()

        for i in range(1,len(self.trail)):
            if self.trail[i][0] == self.SnakeX and self.trail[i][1] == self.SnakeY:
                self.tail -= 1
                self.score -= 1

        self.drawOtherSnakes()

class Site:

    def __init__(self):
        self.user=self.get_me()
        self.best=1000

    def GameOver(self):
        window.clearInterval(game_loop)
        gameSnake.trail=[]
        window.alert("A játéknak sajnos vége.\n Végső pontod:"+str(gameSnake.score))

    def update_score(self,new_score):
        document["score"].innerHTML = "Pontok: " + str(new_score)

        if new_score > gameSnake.high_score:
            document["high-score"].innerHTML = "Legjobb pont: " + str(new_score)
            gameSnake.high_score = new_score
        if new_score<=-4:
            self.GameOver()

    def ranking(self):
        self.user=self.user.replace('%20%20','')
        self.user=self.user.replace(' ','')

        if game.gamePoints:
            numbers = document.getElementById("numbers")
            if numbers:
                numbers.remove()
                div = document.createElement("div")
                div.setAttribute("id", "numbers")
                rank = document.getElementById("ranking")
                rank.appendChild(div)
            
            for i in range(0,len(game.gamePoints['points'])):
                btn = document.createElement("p")
                name = str(game.gamePoints['points'][i][0]).split(" ")[-3]
                btn.innerHTML = str(i+1) + ". " + name + " " + str(game.gamePoints['points'][i][1])
                if name in self.user and str(game.id) == str(game.gamePoints['points'][i][0]).split(" ")[-1]:
                    btn.style.color = 'green'
                    if i+1<self.best:
                        self.best=i+1
                    document["result"].innerHTML = "Eddigi legjobb helyezés: " + str(self.best) + str(".")
                numbers = document.getElementById("numbers")
                numbers.appendChild(btn)


    def key_push(self,evt):
        key = evt.keyCode
        if (key == 37 or key == 65): #left <-
            gameSnake.xv = -1
            gameSnake.yv = 0
        elif (key == 38 or key == 87): #up ^
            gameSnake.xv = 0
            gameSnake.yv = -1
        elif (key == 39 or key == 68): #right ->
            gameSnake.xv = 1
            gameSnake.yv = 0
        elif (key == 40 or key == 83): #down ˇ
            gameSnake.xv = 0
            gameSnake.yv = 1
            
    def show_instructions(self,evt):
        window.alert("A kígyó irányításához használd a nyíl billentyűket, vagy az asdw billentyűket.A rózsaszín kockák a gyümölcsök melyek +1 pontot érnek, a pirosak a bogarak, melyek +2 pontot, de vigyázz a szürkék a bombák, melyek -1pont. Illetve kerüld a barna falakat, melyeknek ütközés a játék azonnali végét jelenti.")

    def get_me(self):
        me=document.getElementById("me").innerHTML
        return me

    def setting_color(self,evt):
        color = document.getElementById("setColor")
        gameSnake.mySnakeColor = color.options[color.selectedIndex].value;
        gameSnake.mySnakeColor = f"{gameSnake.mySnakeColor}"

if __name__ == "__main__":
    gameSnake = Snake()
    site = Site()
    game = Game()

    canvas = document["game-board"]
    ctx = canvas.getContext("2d")

    document.addEventListener("keydown", site.key_push)
    game_loop = window.setInterval(gameSnake.game, 1000/2)

    # Bug handling
    game.BugUpdate()
    window.setInterval(game.BugUpdate, 4000)

    # Fruit handling
    game.FruitUpdate()
    window.setInterval(game.FruitUpdate, 4000)

    # Bomb handling
    game.BombUpdate()
    window.setInterval(game.BombUpdate, 4000)

    # score handling
    game.handleScores()
    window.setInterval(game.handleScores, 3000)

    #players handling
    game.otherPlayers()
    window.setInterval(game.otherPlayers, 1000/2)
    
    #ranking
    window.setInterval(site.ranking, 3000)
    
    game.getWalls()
    
    instructions_btn = document["instructions-btn"]
    instructions_btn.addEventListener("click", site.show_instructions)
    setting_btn = document["setColor"]
    setting_btn.addEventListener("click", site.setting_color)
