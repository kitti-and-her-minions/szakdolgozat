from browser import document, html, window
from javascript import Math

import random

class Snake:
    def __init__(self):
        self.score = 0
        self.high_score = 0
        self.SnakeX = self.SnakeY = 10 
        self.BoardY =  20 
        self.BoardX = 65
        self.xv = +1
        self.yv = 0
        self.trail = []
        self.tail = 5

        self.pre_pause = [0,0]
        self.paused = False
        
        self.Fruits = []
        self.Bugs = []
        self.Bombs = []
        self.Walls = [[1,10],[1,9],[1,8],[60,11],[61,11],[61,12],[20,5],[21,5],[22,5],[23,5],[23,6],[40,10],[41,9],[42,9],[34,15],[35,15],[36,15],[37,15]]

        self.mySnakeColor = "lime"

    def draw(self,array,color):
        ctx.fillStyle = color
        for i in range(0,len(array)):
            ctx.fillRect(array[i][0]*self.BoardY, array[i][1]*self.BoardY, self.BoardY-2, self.BoardY-2)

    ################## Fix Fruits #########################
    def randFixFruits(self):
        Fruits = [[0,0],[0,0],[0,0],[0,0],[0,0]]
        fru = random.sample(range(1, 65), 5)
        for i in range(0,5):
            Fruits[i][0] = fru[i] 
        fru = random.sample(range(1, 20), 5)
        for i in range(0,5):
            Fruits[i][1] = fru[i] 
        return Fruits

    ##################### Bombák #########################
    def randFixBomb(self):
        Bombs = [[0,0],[0,0],[0,0],[0,0],[0,0]]
        b = random.sample(range(1, 65), 5)
        for i in range(0,5):
            Bombs[i][0] = b[i] 
        b = random.sample(range(1, 20), 5)
        for i in range(0,5):
            Bombs[i][1] = b[i] 
        return Bombs

    ############### Moving fruits ##################
    def Bug(self):
        self.Bugs = [[0,0],[0,0],[0,0]]
        fru = random.sample(range(1, 65), 3)
        for i in range(0,3):
            self.Bugs[i][0] = fru[i] 
        fru = random.sample(range(1, 20), 3)
        for i in range(0,3):
            self.Bugs[i][1] = fru[i] 
        ctx.fillStyle = "red"
        for i in range(0,len(self.Bugs)):
            ctx.fillRect(self.Bugs[i][0]*self.BoardY, self.Bugs[i][1]*self.BoardY, self.BoardY-2, self.BoardY-2)

        return self.Bugs

    def moveBugs(self):
        whichDir = Math.floor(Math.random()*2)
        for i in range(0,3):
            if self.Bugs[i][0]>65:
                self.Bugs[i][0]=0+i+1*20
            elif self.Bugs[i][1]>20:
                self.Bugs[i][1]=0+i+1*2
            self.Bugs[i][whichDir]=self.Bugs[i][whichDir]+1

    def changeObjPos(self,array,other,point):
        for i in range(0,len(array)):
            if array[i][0] == self.SnakeX and array[i][1] == self.SnakeY:
                self.tail += point
                array[i][0] = Math.floor(Math.random()*self.BoardX)
                array[i][1] = Math.floor(Math.random()*self.BoardY)
                while array[i] in self.Walls or array[i] in other:
                    array[i][0] = Math.floor(Math.random()*self.BoardX)
                    array[i][1] = Math.floor(Math.random()*self.BoardY)
                self.score += point

    def game(self):
    
        self.SnakeX += gameSnake.xv
        self.SnakeY += gameSnake.yv
        if self.SnakeX < 0:
            self.SnakeX = self.BoardX-1
        if self.SnakeX > self.BoardX-1:
            self.SnakeX = 0
        if self.SnakeY < 0:
            self.SnakeY = self.BoardY-1
        if self.SnakeY > self.BoardY-1:
            self.SnakeY = 0
        
        ctx.fillStyle = "black"
        ctx.fillRect(0, 0, canvas.width, canvas.height)

        ctx.fillStyle = self.mySnakeColor
        for i in range(0,len(self.trail)):
            ctx.fillRect(self.trail[i][0]*self.BoardY, self.trail[i][1]*self.BoardY, self.BoardY-2, self.BoardY-2)

        self.trail.insert(0, [self.SnakeX, self.SnakeY])
        while len(self.trail) > self.tail:
            self.trail.pop()

        self.changeObjPos(self.Fruits,self.Bombs,1)
        self.draw(self.Fruits,"pink")
        self.changeObjPos(self.Bombs,self.Fruits,-1)
        self.draw(self.Bombs,"gray")
        
        for i in range(0,len(self.Bugs)):
            if self.Bugs[i][0] == self.SnakeX and self.Bugs[i][1] == self.SnakeY:
                self.score += 2
                self.tail += 1
                self.Bugs[i][0] = Math.floor(Math.random()*self.BoardX)
                self.Bugs[i][1] = Math.floor(Math.random()*self.BoardY)
        self.draw(self.Bugs,"red")

        self.draw(self.Walls,"brown")

        for i in range(0,len(self.Walls)):
            if self.Walls[i][0] == self.SnakeX and self.Walls[i][1] == self.SnakeY:
                site.GameOver()

        for i in range(1,len(self.trail)):
            if self.trail[i][0] == self.SnakeX and self.trail[i][1] == self.SnakeY:
                if not self.paused:
                    self.tail -= 1
                    self.score -= 1
                    self.score=0

        site.update_score(self.score)


class Site():

    def GameOver(self):
        window.clearInterval(game_loop)
        window.clearInterval(fruit_loop)
        window.alert("A játéknak sajnos vége.\n Végső pontod:"+str(gameSnake.score)+"\n Új játék kezdéséhez kattints az \"OK\"-ra majd nyomd le az \"n\"billentyűt")

    def NewGame(self,evt):
        global fruit_loop, game_loop
        window.clearInterval(fruit_loop)
        window.clearInterval(game_loop)
        gameSnake.score = 0
        gameSnake.high_score = 0
        gameSnake.SnakeX = gameSnake.SnakeY = 10 
        gameSnake.BoardY =  20 
        gameSnake.BoardX = 65
        gameSnake.xv = +1
        gameSnake.yv = 0
        gameSnake.trail = []
        gameSnake.tail = 5

        gameSnake.pre_pause = [0,0]
        gameSnake.paused = False
        game_loop = window.setInterval(gameSnake.game, 1000/2)
        fruit_loop=window.setInterval(gameSnake.moveBugs, 1000/1)

    def update_score(self,new_score):
        document["score"].innerHTML = "Pontok: " + str(new_score)
        if new_score > gameSnake.high_score:
            document["high-score"].innerHTML = "High Score: " + str(new_score)
            gameSnake.high_score = new_score
        if new_score<=-4:
            gameSnake.paused=True
            self.GameOver()

    def key_push(self,evt):
        global fruit_loop, game_loop
        key = evt.keyCode
        if (key == 37 or key == 65) and not gameSnake.paused: #left <-
            gameSnake.xv = -1
            gameSnake.yv = 0
        elif (key == 38 or key == 87) and not gameSnake.paused: #up ^
            gameSnake.xv = 0
            gameSnake.yv = -1
        elif (key == 39 or key == 68) and not gameSnake.paused: #right ->
            gameSnake.xv = 1
            gameSnake.yv = 0
        elif (key == 40 or key == 83) and not gameSnake.paused: #down ˇ
            gameSnake.xv = 0
            gameSnake.yv = 1
        elif key == 32: #space
            temp = [gameSnake.xv, gameSnake.yv]
            gameSnake.xv = gameSnake.pre_pause[0]
            gameSnake.yv = gameSnake.pre_pause[1]
            gameSnake.pre_pause = [*temp]
            if gameSnake.paused:
                fruit_loop=window.setInterval(gameSnake.moveBugs, 1000/1)
            else:
                window.clearInterval(fruit_loop)
            gameSnake.paused = not gameSnake.paused
        elif key == 78: #n as new game
            self.NewGame(evt)
        
    def show_instructions(self,evt):
        window.alert("A kígyó irányításához használd a nyíl billentyűket, vagy az asdw billentyűket.A rózsaszín kockák a gyümölcsök melyek +1 pontot érnek, a pirosak a bogarak, melyek +2 pontot, de vigyázz a szürkék a bombák, melyek -1pont. Kerüld a barna falakat, melyeknek ütközés a játék azonalli végét jelenti. Új játék kezdéséhez kattints az \"Új játék\" gombra vagy szimplán nyomd le az \"n\" billentyűt a játék szüneteléséhez nyomd meg a space gombot.")

    def setting_color(self,evt):
        color = document.getElementById("setColor")
        gameSnake.mySnakeColor = color.options[color.selectedIndex].value;
        gameSnake.mySnakeColor = f"{gameSnake.mySnakeColor}"

if __name__ == "__main__":

    gameSnake = Snake()
    site = Site()

    canvas = document["game-board"]
    ctx = canvas.getContext("2d")

    gameSnake.Fruits = gameSnake.randFixFruits()
    gameSnake.Bombs = gameSnake.randFixBomb()
    gameSnake.Bugs = gameSnake.Bug()

    document.addEventListener("keydown", site.key_push)
    game_loop = window.setInterval(gameSnake.game, 1000/2)

    fruit_loop = window.setInterval(gameSnake.moveBugs, 1000/1)

    instructions_btn = document["instructions-btn"]
    instructions_btn.addEventListener("click", site.show_instructions)
    setting_btn = document["setColor"]
    setting_btn.addEventListener("click", site.setting_color)

    newgame_btn = document["newgame-btn"]
    newgame_btn.addEventListener("click", site.NewGame)

