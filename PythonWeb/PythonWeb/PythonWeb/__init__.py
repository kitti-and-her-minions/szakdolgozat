"""
The flask application package.
"""

from flask import Flask, render_template, request, jsonify, make_response, json

app = Flask(__name__)


import PythonWeb.views
