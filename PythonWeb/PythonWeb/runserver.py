"""
This script runs the PythonWeb application using a development server.
"""


from os import environ
from PythonWeb import app
from PythonWeb.views import bug1,bug2,bug3

if __name__ == '__main__':
    HOST = environ.get('SERVER_HOST', '0.0.0.0')
    try:
        PORT = int(environ.get('SERVER_PORT', '5000'))
    except ValueError:
        PORT = 5555

    bug1.start()
    bug2.start()
    bug3.start()

    app.run(HOST, PORT,threaded=True)

    

